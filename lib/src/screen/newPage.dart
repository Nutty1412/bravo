import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewPage extends StatefulWidget {
  @override
  _NewPageState createState() => _NewPageState();
}

class _NewPageState extends State<NewPage> {
  String id,role,phone;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: StreamBuilder(
          stream: Firestore.instance.collection('news').snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return snapshot.data.documents.length != 0 ? ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, i) {
                  var data = snapshot.data.documents[i];
                  DateTime dateTime = data["time"].toDate();
                  return Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Stack(
                      children: [
                        Card(
                          child: Column(
                            children: [
                              Image.memory(
                                base64Decode(data['image']),
                                gaplessPlayback: true
                              ),
                              Text(
                                data['title'],
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              Text(
                                data['subTitle'],
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              )
                            ],
                          )
                        ),
                        Container(
                          alignment: Alignment.topRight,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(5)
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(12),
                              child: Column(
                                children: [
                                  Text(
                                    DateFormat('HH:mm').format(dateTime),
                                    style: TextStyle(color: Colors.white)
                                  ),
                                  Text(
                                    DateFormat('dd MMM yy').format(dateTime),
                                    style: TextStyle(color: Colors.white)
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ) : Center(
                child: Text('ไม่พบข้อมูลข่าวสาร'),
              );
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
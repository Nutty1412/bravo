import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}

class _LoginpageState extends State<Loginpage> {
  final _formKey = GlobalKey<FormState>();
  String username,password;
  var loginSuccess = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.2,
              ),
              Image.asset(
                'assets/images/logo.jpg',
                width: MediaQuery.of(context).size.width * 0.5,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  'เข้าสู่ระบบชุมชน',
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.05,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                  errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  icon: Icon(Icons.person),
                  labelText: 'Username'
                ),
                onSaved: (value) => username = value,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'กรุณากรอกUsername';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                  errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                  icon: Icon(Icons.vpn_key),
                  labelText: 'รหัสผ่าน'
                ),
                obscureText: true,
                onSaved: (value) => password = value,
                onChanged: (value) {
                  setState(() {
                    loginSuccess = true;
                  });
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return 'กรุณากรอกรหัสผ่าน';
                  } else if (value.length < 6) {
                    return 'กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัว';
                  }
                  return null;
                },
              ),
              if (!loginSuccess)
                Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Text('กรุณาตรวจสอบ Username หรือรหัสผ่านอีกครั้ง',style: TextStyle(color: Colors.red)),
                ),
              SizedBox(height: 20),
              RaisedButton(
                color: Colors.green,
                onPressed: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    await Firestore.instance.collection("users").where("id", isEqualTo: username).where("password", isEqualTo: password)
                    .getDocuments().then((value) {
                        if (value.documents.length == 0) {
                          setState(() {
                            loginSuccess = false;
                          });
                        } else {
                          prefs.setString('uid',value.documents[0].documentID);
                          prefs.setString('house', value.documents[0].data['house']);
                          prefs.setString('id', value.documents[0].data['id']);
                          prefs.setString('role', value.documents[0].data['role']);
                          prefs.setString('phone', value.documents[0].data['phone']);
                          prefs.setString('name', value.documents[0].data['name']);
                          prefs.setString('email', value.documents[0].data['email']);
                          prefs.setString('password', value.documents[0].data['password']);
                          Phoenix.rebirth(context);
                        }
                      }
                    );
                  }
                },
                child: Text(
                  'เข้าสู่ระบบ',
                  style: TextStyle(
                    color: Colors.white
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
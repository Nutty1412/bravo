import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditNew extends StatefulWidget {
  final oldImage,oldTitle,oldSubTitle,uid;

  EditNew({
    @required this.oldImage,
    @required this.oldTitle,
    @required this.oldSubTitle,
    @required this.uid
  });

  @override
  _EditNewState createState() => _EditNewState();
}

class _EditNewState extends State<EditNew> {
  TextEditingController _title = TextEditingController();
  TextEditingController _subtitle = TextEditingController();
  String image,title,subTitle,newImage;
  bool haveTitle = true,haveSubTitle = false;
  @override
  void initState() {
    image = this.widget.oldImage;
    _title.text = this.widget.oldTitle;
    _subtitle.text = this.widget.oldSubTitle;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลข่าวสาร'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: Column(
              children: [
                SizedBox(height: 20),
                InkWell(
                  onTap:  () async {
                    var res = await pickImage();
                    if (res != null) {
                      setState(() {
                        newImage = res;
                      });
                    }
                  },
                  child: newImage != null ? Image.memory(
                    base64Decode(newImage),
                    gaplessPlayback: true
                  ) : Image.memory(
                    base64Decode(image),
                    gaplessPlayback: true
                  ),
                ),
                SizedBox(height: 20),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _title,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    icon: Icon(Icons.fiber_new),
                    labelText: 'หัวข้อข่าว'
                  ),
                  onChanged: (value) {
                    setState(() {
                      title = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'กรุณากรอกหัวข้อข่าว';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _subtitle,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                    icon: Icon(Icons.more),
                    labelText: 'รายละเอียดข่าว'
                  ),
                  onChanged: (value) {
                    setState(() {
                      subTitle = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'กรุณากรอกรายละเอียดข่าว';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                RaisedButton(
                  color: Colors.green,
                  onPressed: () async {
                    await Firestore.instance.collection("news").document(this.widget.uid).updateData({
                      "image" : newImage == null ? image : newImage,
                      "title" : title == null ? _title.text : title,
                      "subTitle" : subTitle == null ? _subtitle.text : subTitle,
                      "time" : DateTime.now()
                    });
                    Navigator.pop(context,'success');
                  },
                  child: Text(
                    'แก้ไขข้อมูลข่าวสาร',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  pickImage() async {
    final ImagePicker _picker = ImagePicker();
    var image = await _picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 500
    );
    if (image != null) {
      List<int> imageBytes = await image.readAsBytes();
      String base64Image = base64Encode(imageBytes);
      return base64Image;
    } else {
      return null;
    }
  }
}
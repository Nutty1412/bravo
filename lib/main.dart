import 'package:emergency/src/App.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await _lockScreenPortraitOrientation();
  runApp(Phoenix(child: MyApp()));
}

Future<void> _lockScreenPortraitOrientation() =>
SystemChrome.setPreferredOrientations([
  DeviceOrientation.portraitUp,
]);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: App(),
    );
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emergency/src/screen/editDataUser.dart';
import 'package:flutter/material.dart';

class EditUser extends StatefulWidget {
  @override
  _EditUserState createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  String image,title,subTitle;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ข้อมูลสมาชิก'),
      ),
      body: SingleChildScrollView(
        child: StreamBuilder(
          stream: Firestore.instance.collection('users').where('role',isEqualTo: 'user').snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return snapshot.data.documents.length != 0 ? ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, i) {
                  var data = snapshot.data.documents[i];
                  return Card(
                    child: ListTile(
                      title: Text(
                        'ชื่อ : ${data['name']}',
                        style: TextStyle(fontSize: 20),
                      ),
                      subtitle: Text(
                        'บ้านเลขที่ : ${data['id']}\nเบอร์ติดต่อ : ${data['phone']}\nอีเมล : ${data['email']}',
                        style: TextStyle(fontSize: 20),
                      ),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EditDataUser(
                        data: data,
                      ))).then((value) => {
                        if (value == 'success') {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("แก้ไขข้อมูล User เรียบร้อยแล้ว"),
                          ))
                        }
                      }),
                    ),
                  );
                },
              ) : Center(
                child: Text('ไม่พบ User'),
              );
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      )
    );
  }
}
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emergency/src/screen/editNew.dart';
import 'package:flutter/material.dart';

class NewPageForAdmin extends StatefulWidget {
  @override
  _NewPageForAdminState createState() => _NewPageForAdminState();
}

class _NewPageForAdminState extends State<NewPageForAdmin> {
  String id,role,phone;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลข่าวสาร'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: StreamBuilder(
            stream: Firestore.instance.collection('news').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return snapshot.data.documents.length != 0 ? ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, i) {
                    var data = snapshot.data.documents[i];
                    return InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => EditNew(
                          oldImage: data['image'],
                          oldSubTitle: data['subTitle'],
                          oldTitle: data['title'],
                          uid: data.documentID,
                        ))).then((value) => {
                          if (value == 'success') {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text("อัพเดตข้อมูลข่าวสารเรียบร้อยแล้ว"),
                            ))
                          }
                        });
                      },
                      child: Card(
                        child: Column(
                          children: [
                            Image.memory(
                              base64Decode(data['image']),
                              gaplessPlayback: true
                            ),
                            Text(
                              data['title'],
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            Text(
                              data['subTitle'],
                              style: TextStyle(
                                fontSize: 20
                              ),
                            )
                          ],
                        )
                      ),
                    );
                  },
                ) : Center(
                  child: Text('ไม่พบข้อมูลข่าวสาร'),
                );
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}
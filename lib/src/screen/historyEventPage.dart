import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryEventPage extends StatefulWidget {
  @override
  _HistoryEventPageState createState() => _HistoryEventPageState();
}

class _HistoryEventPageState extends State<HistoryEventPage> {
  String id,role;
  int select = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 0,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
        TabBar(
          labelColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
          indicatorColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
          unselectedLabelColor: Colors.black,
          onTap: (value) {
            setState(() {
              select = value;
            });
          },
          tabs: [
            Tab(text: 'รอการตรวจสอบ'),
            Tab(text: 'ดำเนินการ'),
            Tab(text: 'เสร็จสิ้น'),
            Tab(text: 'ยกเลิก')
          ],
        ),
        Expanded(
          child: Container(
            child: TabBarView(
              children: <Widget>[
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('events').where('username',isEqualTo: id).where('status',isEqualTo: 'รอการตรวจสอบจาก Admin').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.event),
                                        title: Text('หัวข้อ :: ' + data['event']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.home),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.calendar_today),
                                        title: Text('วันที่ :: ' + data['date']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('เวลา :: ' + data['time']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                      ),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('events').where('username',isEqualTo: id).where('status',isEqualTo: 'กำลังดำเนินการ').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.event),
                                        title: Text('หัวข้อ :: ' + data['event']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.home),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.calendar_today),
                                        title: Text('วันที่ :: ' + data['date']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('เวลา :: ' + data['time']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                      ),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('events').where('username',isEqualTo: id).where('status',isEqualTo: 'เสร็จสิ้น').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.event),
                                        title: Text('หัวข้อ :: ' + data['event']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.home),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.calendar_today),
                                        title: Text('วันที่ :: ' + data['date']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('เวลา :: ' + data['time']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                      ),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('events').where('username',isEqualTo: id).where('status',isEqualTo: 'ยกเลิก').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.event),
                                        title: Text('หัวข้อ :: ' + data['event']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.home),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.calendar_today),
                                        title: Text('วันที่ :: ' + data['date']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('เวลา :: ' + data['time']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                      ),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ]
            )
          ),
        )
      ])
    );
  }

  _getUserLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return [{
      "id" : prefs.getString('id'),
      "role" : prefs.getString('role')
    }];
  }
}
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserOpenTicketPage extends StatefulWidget {
  final username,phone,name;
  UserOpenTicketPage({
    @required this.username,
    @required this.phone,
    @required this.name
  });
  @override
  _UserOpenTicketPageState createState() => _UserOpenTicketPageState();
}

class _UserOpenTicketPageState extends State<UserOpenTicketPage> {
  final _formKey = GlobalKey<FormState>();
  final key = new GlobalKey<ScaffoldState>();
  String username,phone,emergency,etc = '',location;
  File file;
  String fileName = '',url = '';
  var item = [
    'ไฟไหม้',
    'พบสัตว์เลื้อยคลาน',
    'ต้องการรถฉุกเฉิน',
    'ทะเลาะวิวาท',
    'ไฟฟ้าลัดวงจร',
    'อื่น ๆ'
  ];
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แจ้งเหตุฉุกเฉิน'),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/images/emergency.png'),
              Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    TextFormField(
                      initialValue: this.widget.username,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.person),
                        labelText: 'บ้านเลขที่'
                      ),
                      enabled: false,
                      onSaved: (value) => username = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกบ้านเลขที่';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      initialValue: this.widget.name,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.person),
                        labelText: 'ชื่อผู้แจ้ง'
                      ),
                      onSaved: (value) => phone = value,
                      keyboardType: TextInputType.phone,
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      initialValue: this.widget.phone,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.phone),
                        labelText: 'เบอร์โทรศัพท์'
                      ),
                      onSaved: (value) => phone = value,
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกเบอร์โทรศัพท์';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    DropdownButtonFormField(
                      items: item.map((label) => DropdownMenuItem(
                        child: Text(label.toString()),
                        value: label,
                      )).toList(),
                      onSaved: (value) => emergency = value,
                      onChanged: (value) {
                        setState(() {
                          emergency = value;
                        });
                      },
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        icon: Icon(Icons.warning),
                        labelText: 'เหตุการณ์ที่เจอ'
                      ),
                    ),
                    if(emergency == 'อื่น ๆ')
                    Column(
                      children: [
                        SizedBox(height: 15),
                        TextFormField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                            errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                            focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                            icon: Icon(Icons.more),
                            labelText: 'รายละเอียดเพิ่มเติม'
                          ),
                          onSaved: (value) => etc = value,
                        ),
                      ],
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.zoom_in),
                        labelText: 'ตำแหน่งเกิดเหตุ'
                      ),
                      onSaved: (value) => location = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกตำแหน่งเกิดเหตุ';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Column(
                        children: <Widget>[
                          fileName == '' ? RaisedButton(
                            color: Theme.of(context).primaryColor,
                            onPressed: () async {
                              file = await FilePicker.getFile(type: FileType.image);
                              print(file);
                              if (file != null) {
                                setState(() {
                                  fileName = p.basename(file.path);
                                });
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                "อัพโหลดรูปภาพประกอบ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16
                                ),
                              ),
                            ),
                          ) : Column(
                            children: <Widget>[
                              Center(child: Image.file(file)),
                              RaisedButton(
                                color: Theme.of(context).primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                onPressed: () async {
                                  var backupfile = file;
                                  file = await FilePicker.getFile(type: FileType.image);
                                  if (file != null) {
                                    setState(() {
                                      fileName = p.basename(file.path);
                                    });
                                  } else {
                                    setState(() {
                                      file = backupfile;
                                    });
                                  }
                                },
                                child: Text(
                                  "แก้ไขรูปภาพประกอบ",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16
                                  ),
                                ),
                              )
                            ],
                          ),
                        ]
                      )
                    ),
                    SizedBox(height: 15),
                    loading == true ? CircularProgressIndicator() : RaisedButton(
                      onPressed: () async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String house = prefs.getString('house');
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            loading = true;
                          });
                          _formKey.currentState.save();
                          if (fileName != '') {
                            StorageReference storageReference;
                            storageReference = FirebaseStorage.instance.ref().child("emergency/$fileName");
                            final StorageUploadTask uploadTask = storageReference.putFile(file);
                            final StorageTaskSnapshot downloadUrl = (await uploadTask.onComplete);
                            url = (await downloadUrl.ref.getDownloadURL());
                          }
                          Firestore.instance.collection("tickets").document().setData({
                            "house" : house,
                            "picture" : url,
                            "username" : username,
                            "phone" : phone,
                            "emergency" : emergency,
                            "etc" : etc,
                            "location" : location,
                            "time" : DateTime.now(),
                            "status" : "รอการตรวจสอบจาก Admin"
                          });
                          Dio _dio = Dio();
                          String fileName2;
                          if (file != null) {
                            fileName2 = file.path.split('/').last;
                          }
                          _dio.options.contentType= Headers.formUrlEncodedContentType;
                          var formData = FormData.fromMap({
                            'message': 'username : $username ได้แจ้งเหตุ $emergency ณ ตำแหน่ง $location สามารถติดต่อได้ที่ $phone กรุณาเข้าแอพเพื่อตรวจสอบ',
                            'imageFile': file == null ? '' : await MultipartFile.fromFile(file.path,filename: fileName2)
                          });
                          _dio.options.contentType= Headers.formUrlEncodedContentType;
                          _dio.post(
                            'https://notify-api.line.me/api/notify',
                            data: formData,
                            options: Options(
                              headers: {
                                "authorization": "Bearer xmfG4f1NrLPQKQ6uleTYsLMmOmWoOp4ZLuDI6XJI7Mq"
                              },
                              contentType: Headers.formUrlEncodedContentType
                            ),
                          );
                          Navigator.pop(context,'success');
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Text(
                          'แจ้งเหตุ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color: Colors.red,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}
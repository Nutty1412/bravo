import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserOpenEventPage extends StatefulWidget {
  final username,phone;
  UserOpenEventPage({
    @required this.username,
    @required this.phone
  });
  @override
  _UserOpenEventPageState createState() => _UserOpenEventPageState();
}

class _UserOpenEventPageState extends State<UserOpenEventPage> {
  TextEditingController dateTime = TextEditingController();
  TextEditingController timeStamp = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final key = new GlobalKey<ScaffoldState>();
  var item = [
    'ขอใช้พื้นที่ส่วนกลาง',
    'ขอจัดกิจกรรมภายในชุมชน'
  ];
  @override
  Widget build(BuildContext context) {
    String username,date,time,phone,event,etc = '';
    return Scaffold(
      appBar: AppBar(
        title: Text('ขอใช้พื้นที่ส่วนกลาง'),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/images/event.jpg'),
              Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    TextFormField(
                      initialValue: this.widget.username,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.person),
                        labelText: 'บ้านเลขที่'
                      ),
                      enabled: false,
                      onSaved: (value) => username = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกบ้านเลขที่';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      initialValue: this.widget.phone,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.phone),
                        labelText: 'เบอร์โทรศัพท์'
                      ),
                      onSaved: (value) => phone = value,
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกเบอร์โทรศัพท์';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      controller: dateTime,
                      onTap: () async {
                        DateTime newDateTime = await showRoundedDatePicker(
                          theme: ThemeData(
                            primaryColor: Theme.of(context).primaryColor,
                            accentColor: Theme.of(context).primaryColor
                          ),
                          context: context,
                          era: EraMode.BUDDHIST_YEAR,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now().subtract(Duration(days: 3)),
                          lastDate: DateTime.now().add(Duration(days: 15)),
                        );
                        if (newDateTime != null) {
                          FocusScope.of(context).requestFocus(FocusNode());
                          setState(() => dateTime.text = formatDate(newDateTime, [dd,'/', mm,'/', yyyy]).toString());
                        }
                      },
                      initialValue: phone,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.date_range),
                        labelText: 'วันที่จัดงาน',
                        hintText: '01/01/2564'
                      ),
                      onSaved: (value) => date = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกวันที่จัดงาน';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      controller: timeStamp,
                      initialValue: phone,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.date_range),
                        labelText: 'เวลาที่จัดงาน',
                        hintText: 'xx:xx'
                      ),
                      onTap: () async {
                        TimeOfDay timePicked = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.now(),
                        );
                        if (timePicked != null) {
                          FocusScope.of(context).requestFocus(FocusNode());
                          setState(() => timeStamp.text = timePicked.hour.toString() + " : " + timePicked.minute.toString());
                        }
                      },
                      onSaved: (value) => time = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกเวลาที่จัดงาน';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      initialValue: 'ขอใช้พื้นที่ส่วนกลาง',
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.event),
                      ),
                      enabled: false,
                      onSaved: (value) => event = value,
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                        disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                        icon: Icon(Icons.more),
                        labelText: 'รายละเอียดเพิ่มเติม'
                      ),
                      onSaved: (value) => etc = value,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกรายละเอียดเพิ่มเติม';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15),
                    RaisedButton(
                      onPressed: () async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String house = prefs.getString('house');
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          Firestore.instance.collection("events").document().setData({
                            "house" : house,
                            "username" : username,
                            "date" : date,
                            "time" : time,
                            "phone" : phone,
                            "event" : event,
                            "etc" : etc,
                            "status" : "รอการตรวจสอบจาก Admin"
                          });
                          Dio _dio = Dio();
                          _dio.options.contentType= Headers.formUrlEncodedContentType;
                          _dio.post(
                            'https://notify-api.line.me/api/notify',
                            data: {'message': 'มีคนแจ้งงานอีเว้นเข้ามา กรุณาเข้าแอพเพื่อตรวจสอบ'},
                            options: Options(
                              headers: {
                                "authorization": "Bearer xmfG4f1NrLPQKQ6uleTYsLMmOmWoOp4ZLuDI6XJI7Mq"
                              },
                              contentType: Headers.formUrlEncodedContentType
                            ),
                          );
                          Navigator.pop(context,'success');
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Text(
                          'ยืนยัน',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20
                          ),
                        ),
                      ),
                      color: Colors.blue,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}
import 'package:emergency/src/screen/addNew.dart';
import 'package:emergency/src/screen/editPasswordUser.dart';
import 'package:emergency/src/screen/newPageForAdmin.dart';
import 'package:emergency/src/screen/register.dart';
import 'package:flutter/material.dart';

import 'editUser.dart';

class AdminManageUser extends StatefulWidget {
  AdminManageUser({Key key}) : super(key: key);

  @override
  _AdminManageUserState createState() => _AdminManageUserState();
}

class _AdminManageUserState extends State<AdminManageUser> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Card(
            child: ListTile(
              title: Text('ลงทะเบียนสมาชิกใหม่'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Register())).then((value) => {
                if (value == 'success') {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("สมัครสมาชิกใหม่เรียบร้อยแล้ว"),
                  ))
                }
              }),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('แก้ไขข้อมูลสมาชิก'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EditUser()))
            ),
          ),
          Card(
            child: ListTile(
              title: Text('แก้ไขรหัสผ่านของสมาชิก'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EditPasswordUser())).then((value) => {
                if (value == 'success') {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("แก้ไขรหัสผ่านของสมาชิกเรียบร้อยแล้ว"),
                  ))
                }
              })
            ),
          ),
          Card(
            child: ListTile(
              title: Text('อัพเดตข้อมูลข่าวสาร'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddNew())).then((value) => {
                if (value == 'success') {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("อัพเดตข้อมูลข่าวสารเรียบร้อยแล้ว"),
                  ))
                }
              }),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('แก้ไขข้อมูลข่าวสาร'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => NewPageForAdmin()))
            ),
          )
        ],
      ),
    );
  }
}
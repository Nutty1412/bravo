import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AdminManageEvent extends StatefulWidget {
  @override
  _AdminManageEventState createState() => _AdminManageEventState();
}

class _AdminManageEventState extends State<AdminManageEvent> {
  int select = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 0,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
        Container(
          child: TabBar(
            labelColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
            indicatorColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
            unselectedLabelColor: Colors.black,
            onTap: (value) {
              setState(() {
                select = value;
              });
            },
            tabs: [
              Tab(text: 'รอการตรวจสอบ'),
              Tab(text: 'ดำเนินการ'),
              Tab(text: 'เสร็จสิ้น'),
              Tab(text: 'ยกเลิก')
            ],
          ),
        ),
        Expanded(
          child: TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('events').where('status',isEqualTo: 'รอการตรวจสอบจาก Admin').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.event),
                                  title: Text('หัวข้อ :: ' + data['event']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('งาน :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.home),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.calendar_today),
                                  title: Text('วันที่ :: ' + data['date']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('เวลา :: ' + data['time']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                ),
                                Divider(thickness: 1.5),
                                Column(
                                  children: [
                                    Text(
                                      'อนุมัติการตรวจสอบ',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      )
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.check,color: Colors.green),
                                          onPressed: (){
                                            Firestore.instance.collection("events").document(data.documentID).updateData({
                                              "status" : "กำลังดำเนินการ"
                                            });
                                          }
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.block,color: Colors.red),
                                          onPressed: (){
                                            Firestore.instance.collection("events").document(data.documentID).updateData({
                                              "status" : "ยกเลิก"
                                            });
                                          }
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('events').where('status',isEqualTo: 'กำลังดำเนินการ').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.event),
                                  title: Text('หัวข้อ :: ' + data['event']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('งาน :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.home),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.calendar_today),
                                  title: Text('วันที่ :: ' + data['date']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('เวลา :: ' + data['time']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                ),
                                Divider(thickness: 1.5),
                                Column(
                                  children: [
                                    Text(
                                      'ดำเนินการเสร็จสิ้น',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      )
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.check,color: Colors.green),
                                          onPressed: (){
                                            Firestore.instance.collection("events").document(data.documentID).updateData({
                                              "status" : "เสร็จสิ้น"
                                            });
                                          }
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('events').where('status',isEqualTo: 'เสร็จสิ้น').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.event),
                                  title: Text('หัวข้อ :: ' + data['event']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('งาน :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.home),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.calendar_today),
                                  title: Text('วันที่ :: ' + data['date']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('เวลา :: ' + data['time']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                ),
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('events').where('status',isEqualTo: 'ยกเลิก').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.event),
                                  title: Text('หัวข้อ :: ' + data['event']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('งาน :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.home),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.calendar_today),
                                  title: Text('วันที่ :: ' + data['date']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('เวลา :: ' + data['time']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรติดต่อ :: ' + data['phone']),
                                ),
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              )
            ]
          )
        )
      ])
    );
  }
}
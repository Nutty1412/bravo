import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  final username,name,phone,email;

  EditProfile({
    @required this.username,
    @required this.name,
    @required this.phone,
    @required this.email
  });

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  String username,name,phone,email;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลส่วนตัว'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    enabled: false,
                    initialValue: this.widget.username,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.person),
                      labelText: 'บ้านเลขที่'
                    ),
                    onSaved: (value) => username = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกบ้านเลขที่';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.name,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.person),
                      labelText: 'ชื่อ / นามสกุล'
                    ),
                    onSaved: (value) => name = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกชื่อ / นามสกุล';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.phone,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.phone),
                      labelText: 'เบอร์โทรติดต่อ'
                    ),
                    onSaved: (value) => phone = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกเบอร์โทรติดต่อ';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.email,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.email),
                      labelText: 'อีเมล'
                    ),
                    onSaved: (value) => email = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกอีเมล';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String uid = prefs.getString('uid');
                        prefs.setString('phone', phone);
                        prefs.setString('name', name);
                        prefs.setString('email', email);
                        await Firestore.instance.collection("users").document(uid).updateData({
                          "phone" : phone,
                          "name" : name,
                          "email" : email
                        });
                        Phoenix.rebirth(context);
                      }
                    },
                    child: Text(
                      'แก้ไข',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditPasswordUser extends StatefulWidget {
  @override
  _EditPasswordUserState createState() => _EditPasswordUserState();
}

class _EditPasswordUserState extends State<EditPasswordUser> {
  final _formKey = GlobalKey<FormState>();
  String password,username;
  bool editFailed = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('อัพเดตรหัสผ่านของสมาชิก'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.person),
                      labelText: 'บ้านเลขที่ที่ต้องการแก้ไข'
                    ),
                    onSaved: (value) => username = value,
                    onChanged: (_) {
                      setState(() {
                        editFailed = false;
                      });
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกบ้านเลขที่';
                      }
                      return null;
                    },
                  ),
                  if (editFailed)
                    Column(
                      children: [
                        SizedBox(height: 5),
                        Padding(
                          padding: EdgeInsets.only(left: 40),
                          child: Text(
                            'ไม่พบบ้านเลขที่นี้ในระบบ',
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                      ],
                    ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.vpn_key),
                      labelText: 'รหัสผ่านที่ต้องการแก้ไข'
                    ),
                    obscureText: true,
                    onSaved: (value) => password = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกรหัสผ่าน';
                      } else if (value.length < 6) {
                        return 'กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: RaisedButton(
                      color: Colors.green,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          Firestore.instance.collection("users").where("id",isEqualTo: username).getDocuments().then((value) {
                            if (value.documents.length == 0) {
                              setState(() {
                                editFailed = true;
                              });
                            } else {
                              Firestore.instance.collection("users").document(value.documents[0].documentID).updateData({
                                'password' : password
                              }).then((value) {
                                Navigator.pop(context,'success');
                              });
                            }
                          });
                        }
                      },
                      child: Text(
                        'แก้ไขรหัสผ่าน',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
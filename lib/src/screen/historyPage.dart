import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  String id,role;
  int select = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 0,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
        TabBar(
          labelColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
          indicatorColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
          unselectedLabelColor: Colors.black,
          onTap: (value) {
            setState(() {
              select = value;
            });
          },
          tabs: [
            Tab(text: 'รอการตรวจสอบ'),
            Tab(text: 'ดำเนินการ'),
            Tab(text: 'เสร็จสิ้น'),
            Tab(text: 'ยกเลิก')
          ],
        ),
        Expanded(
          child: Container(
            child: TabBarView(
              children: <Widget>[
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('tickets').where('username',isEqualTo: id).where('status',isEqualTo: 'รอการตรวจสอบจาก Admin').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                Timestamp t = data['time'];
                                DateTime d = t.toDate();
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.person),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.warning),
                                        title: Text(data['emergency']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc'].toString()),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('สถานะ :: ' + data['status']),
                                      ),
                                      if (data['picture'] != '')
                                        Image.network(data['picture']),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('tickets').where('username',isEqualTo: id).where('status',isEqualTo: 'กำลังดำเนินการ').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                Timestamp t = data['time'];
                                DateTime d = t.toDate();
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.person),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.warning),
                                        title: Text(data['emergency']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('สถานะ :: ' + data['status']),
                                      ),
                                      if (data['picture'] != '')
                                        Image.network(data['picture']),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('tickets').where('username',isEqualTo: id).where('status',isEqualTo: 'เสร็จสิ้น').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                Timestamp t = data['time'];
                                DateTime d = t.toDate();
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.person),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.warning),
                                        title: Text(data['emergency']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('สถานะ :: ' + data['status']),
                                      ),
                                      if (data['picture'] != '')
                                        Image.network(data['picture']),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder(
                  future: _getUserLogin(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      id = snapshot.data[0]['id'];
                      role = snapshot.data[0]['role'];
                      return StreamBuilder(
                        stream: Firestore.instance.collection('tickets').where('username',isEqualTo: id).where('status',isEqualTo: 'ยกเลิก').snapshots(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data.documents.length != 0 ? ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, i) {
                                var data = snapshot.data.documents[i];
                                Timestamp t = data['time'];
                                DateTime d = t.toDate();
                                return Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.person),
                                        title: Text('บ้านเลขที่ :: ' + data['house']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.phone),
                                        title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.warning),
                                        title: Text(data['emergency']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.more),
                                        title: Text('รายละเอียด :: ' + data['etc']),
                                      ),
                                      ListTile(
                                        leading: Icon(Icons.timer),
                                        title: Text('สถานะ :: ' + data['status']),
                                      ),
                                      if (data['picture'] != '')
                                        Image.network(data['picture']),
                                    ],
                                  )
                                );
                              },
                            ) : Center(
                              child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                            );
                          }
                          return CircularProgressIndicator();
                        },
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ]
            )
          ),
        )
      ])
    );
  }

  _getUserLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return [{
      "id" : prefs.getString('id'),
      "role" : prefs.getString('role')
    }];
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AdminManageEmergency extends StatefulWidget {
  @override
  _AdminManageEmergencyState createState() => _AdminManageEmergencyState();
}

class _AdminManageEmergencyState extends State<AdminManageEmergency> {
  int select = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: 0,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
        Container(
          child: TabBar(
            labelColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
            indicatorColor: select == 0 ? Color(0xFFF7CF1D) : select == 1 ? Colors.red : select == 2 ? Colors.green : Colors.orange,
            unselectedLabelColor: Colors.black,
            onTap: (value) {
              setState(() {
                select = value;
              });
            },
            tabs: [
              Tab(text: 'รอการตรวจสอบ'),
              Tab(text: 'ดำเนินการ'),
              Tab(text: 'เสร็จสิ้น'),
              Tab(text: 'ยกเลิก')
            ],
          ),
        ),
        Expanded(
          child: TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('tickets').where('status',isEqualTo: 'รอการตรวจสอบจาก Admin').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          Timestamp t = data['time'];
                          DateTime d = t.toDate();
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                ),
                                ListTile(
                                  leading: Icon(Icons.person),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.warning),
                                  title: Text(data['emergency']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('รายละเอียด :: ' + data['etc'].toString()),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('สถานะ :: ' + data['status']),
                                ),
                                if (data['picture'] != '')
                                  Image.network(data['picture']),
                                Divider(thickness: 1.5),
                                Column(
                                  children: [
                                    Text(
                                      'อนุมัติการตรวจสอบ',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      )
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.check,color: Colors.green),
                                          onPressed: (){
                                            Firestore.instance.collection("tickets").document(data.documentID).updateData({
                                              "status" : "กำลังดำเนินการ"
                                            });
                                          }
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.block,color: Colors.red),
                                          onPressed: (){
                                            Firestore.instance.collection("tickets").document(data.documentID).updateData({
                                              "status" : "ยกเลิก"
                                            });
                                          }
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('tickets').where('status',isEqualTo: 'กำลังดำเนินการ').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          Timestamp t = data['time'];
                          DateTime d = t.toDate();
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                ),
                                ListTile(
                                  leading: Icon(Icons.person),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.warning),
                                  title: Text(data['emergency']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('รายละเอียด :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('สถานะ :: ' + data['status']),
                                ),
                                if (data['picture'] != '')
                                  Image.network(data['picture']),
                                Divider(thickness: 1.5),
                                Column(
                                  children: [
                                    Text(
                                      'ดำเนินการเสร็จสิ้น',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold
                                      )
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.check,color: Colors.green),
                                          onPressed: (){
                                            Firestore.instance.collection("tickets").document(data.documentID).updateData({
                                              "status" : "เสร็จสิ้น"
                                            });
                                          }
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.block,color: Colors.red),
                                          onPressed: (){
                                            Firestore.instance.collection("tickets").document(data.documentID).updateData({
                                              "status" : "ยกเลิก"
                                            });
                                          }
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('tickets').where('status',isEqualTo: 'เสร็จสิ้น').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          Timestamp t = data['time'];
                          DateTime d = t.toDate();
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                ),
                                ListTile(
                                  leading: Icon(Icons.person),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.warning),
                                  title: Text(data['emergency']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('รายละเอียด :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('สถานะ :: ' + data['status']),
                                ),
                                if (data['picture'] != '')
                                  Image.network(data['picture']),
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
              SingleChildScrollView(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('tickets').where('status',isEqualTo: 'ยกเลิก').snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data.documents.length != 0 ? ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, i) {
                          var data = snapshot.data.documents[i];
                          Timestamp t = data['time'];
                          DateTime d = t.toDate();
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('วันที่เกิดเหตุ :: ' + d.toString()),
                                ),
                                ListTile(
                                  leading: Icon(Icons.person),
                                  title: Text('บ้านเลขที่ :: ' + data['house']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.phone),
                                  title: Text('เบอร์โทรศัพท์ :: ' + data['phone']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.warning),
                                  title: Text(data['emergency']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.more),
                                  title: Text('รายละเอียด :: ' + data['etc']),
                                ),
                                ListTile(
                                  leading: Icon(Icons.timer),
                                  title: Text('สถานะ :: ' + data['status']),
                                ),
                                if (data['picture'] != '')
                                  Image.network(data['picture']),
                              ],
                            )
                          );
                        },
                      ) : Center(
                        child: Text('ไม่พบประวัติการแจ้งเหตุ'),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              )
            ]
          )
        )
      ])
    );
  }
}
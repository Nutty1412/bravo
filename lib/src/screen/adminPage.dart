import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'adminManageEmergency.dart';
import 'adminManageEvent.dart';
import 'adminManageUser.dart';

class Adminpage extends StatefulWidget {
  @override
  _AdminpageState createState() => _AdminpageState();
}

class _AdminpageState extends State<Adminpage> {
  int _selectedIndex = 0;
  var _widgetOptions = <Widget>[
    AdminManageEmergency(),
    AdminManageEvent(),
    AdminManageUser()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แอพภายในชุมชนแสนสุข'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: Icon(Icons.logout),
        onPressed: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.remove('id');
          prefs.remove('role');
          prefs.remove('name');
          prefs.remove('phone');
          prefs.remove('email');
          prefs.remove('password');
          Phoenix.rebirth(context);
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.dangerous),
            label: 'อัพเดตเหตุฉุกเฉิน',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            label: 'อัพเดตงานในชุมชน',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.admin_panel_settings),
            label: 'ระบบ Admin',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onItemTapped,
      )
    );
  }
}
import 'package:emergency/src/screen/userOpenEventPage.dart';
import 'package:emergency/src/screen/userOpenTicketPage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'editPassword.dart';
import 'editProfile.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String id,role,phone,name,email,password,house;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getUserLogin(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          house = snapshot.data[0]['house'];
          id = snapshot.data[0]['id'];
          role = snapshot.data[0]['role'];
          phone = snapshot.data[0]['phone'];
          name = snapshot.data[0]['name'];
          email = snapshot.data[0]['email'];
          password = snapshot.data[0]['password'];
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'ยินดีต้อนรับ $name\nบ้านเลขที่ $house',
                  style: TextStyle(
                    fontSize: 20
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => UserOpenTicketPage(
                        name:name,
                        username: id,
                        phone: phone,
                      ))).then((value) {
                        if (value != null) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("แจ้งเหตุเข้าในระบบเรียบร้อยแล้ว"),
                          ));
                        }
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'แจ้งเหตุฉุกเฉิน',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color: Colors.red,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => UserOpenEventPage(
                        username: id,
                        phone: phone,
                      ))).then((value) {
                        if (value != null) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("แจ้งเหตุเข้าในระบบเรียบร้อยแล้ว"),
                          ));
                        }
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'ขอใช้พื้นที่ส่วนกลาง',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color: Colors.blue,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile(
                        email: email,
                        name: name,
                        phone: phone,
                        username: id,
                      )));
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'แก้ไขข้อมูลส่วนตัว',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color: Colors.pink,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => EditPassword())).then((value) {
                        if (value != null) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("แก้ไขรหัสผ่านเรียบร้อยแล้ว"),
                          ));
                        }
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'แก้ไขข้อมูลรหัสผ่าน',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                        ),
                      ),
                    ),
                    color: Colors.purple,
                  ),
                ),
              ],
            ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  _getUserLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return [{
      "house" : prefs.getString('house'),
      "id" : prefs.getString('id'),
      "role" : prefs.getString('role'),
      "phone" : prefs.getString('phone'),
      "name" :prefs.getString('name'),
      "email" : prefs.getString('email'),
      "passwoard" : prefs.getString('password')
    }];
  }
}
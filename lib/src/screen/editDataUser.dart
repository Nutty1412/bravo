import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditDataUser extends StatefulWidget {
  final data;
  EditDataUser({
    @required this.data
  });
  @override
  _EditDataUserState createState() => _EditDataUserState();
}

class _EditDataUserState extends State<EditDataUser> {
  final _formKey = GlobalKey<FormState>();
  String username,password,name,phone,email;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลสมาชิก'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    initialValue: this.widget.data['id'],
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.person),
                      labelText: 'บ้านเลขที่'
                    ),
                    onSaved: (value) => username = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกบ้านเลขที่';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.data['name'],
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.person),
                      labelText: 'ชื่อ / นามสกุล'
                    ),
                    onSaved: (value) => name = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกชื่อ / นามสกุล';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.data['phone'],
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.phone),
                      labelText: 'เบอร์โทรติดต่อ'
                    ),
                    onSaved: (value) => phone = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกเบอร์โทรติดต่อ';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    initialValue: this.widget.data['email'],
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.email),
                      labelText: 'อีเมล'
                    ),
                    onSaved: (value) => email = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกอีเมล';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        await Firestore.instance.collection("users").document(this.widget.data.documentID).updateData({
                          "id" : username,
                          "phone" : phone,
                          "name" : name,
                          "email" : email
                        });
                        Navigator.pop(context,'success');
                      }
                    },
                    child: Text(
                      'อัพเดต',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
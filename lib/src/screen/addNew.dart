import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddNew extends StatefulWidget {
  @override
  _AddNewState createState() => _AddNewState();
}

class _AddNewState extends State<AddNew> {
  final _formKey = GlobalKey<FormState>();
  String image,title,subTitle;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่มข้อมูลข่าวสาร'),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  InkWell(
                    onTap:  () async {
                      var res = await pickImage();
                      if (res != null) {
                        setState(() {
                          image = res;
                        });
                      }
                    },
                    child: image == null ? Container(
                      width: MediaQuery.of(context).size.width * 0.75,
                      height: MediaQuery.of(context).size.width * 0.75,
                      color: Colors.grey,
                      child: Center(
                        child: Text(
                          'แนบรูป',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 32
                          ),
                        )
                      ),
                    ) : Image.memory(
                      base64Decode(image),
                      gaplessPlayback: true
                    ),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.fiber_new),
                      labelText: 'หัวข้อข่าว'
                    ),
                    onSaved: (value) => title = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกหัวข้อข่าว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.more),
                      labelText: 'รายละเอียดข่าว'
                    ),
                    onSaved: (value) => subTitle = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกรายละเอียดข่าว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        await Firestore.instance.collection("news").document().setData({
                          "image" : image,
                          "title" : title,
                          "subTitle" : subTitle,
                          "time" : DateTime.now()
                        });
                        Navigator.pop(context,'success');
                      }
                    },
                    child: Text(
                      'เพิ่มข่าวสาร',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  pickImage() async {
    final ImagePicker _picker = ImagePicker();
    var image = await _picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 500
    );
    if (image != null) {
      List<int> imageBytes = await image.readAsBytes();
      String base64Image = base64Encode(imageBytes);
      return base64Image;
    } else {
      return null;
    }
  }
}
import 'package:emergency/src/screen/adminPage.dart';
import 'package:emergency/src/screen/loginPage.dart';
import 'package:emergency/src/screen/userPage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder(
        future: _getUserLogin(),
        builder: (context, snapshot) {
          if (snapshot.data == 'user') {
            return UserPage();
          } else if (snapshot.data == 'admin') {
            return Adminpage();
          } else if (snapshot.data == null)  {
            return Loginpage();
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      )
    );
  }

  _getUserLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('role') == 'user') {
      return 'user';
    } else if (prefs.getString('role') == 'admin') {
      return 'admin';
    }
    return null;
  }
}
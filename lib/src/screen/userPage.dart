import 'package:emergency/src/screen/welcomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'historyEventPage.dart';
import 'historyPage.dart';
import 'newPage.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  int _selectedIndex = 0;
  var _widgetOptions = <Widget>[
    WelcomePage(),
    HistoryPage(),
    HistoryEventPage(),
    NewPage()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แอพภายในชุมชนแสนสุข'),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: Icon(Icons.logout),
        onPressed: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.remove('id');
          prefs.remove('role');
          prefs.remove('name');
          prefs.remove('phone');
          prefs.remove('email');
          prefs.remove('password');
          Phoenix.rebirth(context);
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'หน้าหลัก',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            label: 'การแจ้งเหตุ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            label: 'การแจ้งอีเว้น',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.new_releases),
            label: 'ข่าวสารในชุมชน',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        unselectedLabelStyle: TextStyle(color: Colors.grey),
        onTap: _onItemTapped,
      )
    );
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditPassword extends StatefulWidget {
  EditPassword({Key key}) : super(key: key);

  @override
  _EditPasswordState createState() => _EditPasswordState();
}

class _EditPasswordState extends State<EditPassword> {
  String oldPassword,newPassword,confirmPassword;
  TextEditingController _oldPassword = TextEditingController();
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แก้ไขข้อมูลรหัสผ่าน'),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _oldPassword,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.vpn_key),
                      labelText: 'รหัสผ่านเก่า'
                    ),
                    obscureText: true,
                    onChanged: (value) => oldPassword = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกรหัสผ่าน';
                      } else if (value.length < 6) {
                        return 'กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  Divider(color: Colors.black54),
                  SizedBox(height: 20),
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _newPassword,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.vpn_key),
                      labelText: 'รหัสผ่านใหม่'
                    ),
                    obscureText: true,
                    onChanged: (value) => newPassword = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกรหัสผ่าน';
                      } else if (value.length < 6) {
                        return 'กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _confirmPassword,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.green)),
                      icon: Icon(Icons.vpn_key),
                      labelText: 'ยืนยันรหัสผ่าน'
                    ),
                    obscureText: true,
                    onChanged: (value) => confirmPassword = value,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'กรุณากรอกรหัสผ่าน';
                      } else if (value.length < 6) {
                        return 'กรุณาระบุรหัสผ่านอย่างน้อย 6 ตัว';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 20),
                  RaisedButton(
                    color: Colors.green,
                    onPressed: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      String password,uid;
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      password = prefs.getString('password');
                      uid = prefs.getString('uid');
                      print(password);
                      print(oldPassword);
                      if (password != oldPassword) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("รหัสผ่านเก่าไม่ถูกต้อง"),
                        ));
                      } else if (newPassword != confirmPassword) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("รหัสผ่านใหม่กับยืนยันรหัสผ่านไม่ตรงกัน"),
                        ));
                      } else {
                        await Firestore.instance.collection("users").document(uid).updateData({
                          "password" : newPassword,
                        });
                        Navigator.pop(context,'success');
                      }
                    },
                    child: Text(
                      'แก้ไขรหัสผ่าน',
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}